package com.example.indoor;

import java.util.ArrayList;
import java.util.List;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import com.example.gl.SensorVector;
import com.example.indoor.capture.CvCaptureDevice;
import com.example.indoor.capture.ICaptureDevice;
import com.example.indoor.capture.CaptureWorker.MissingDeviceException;
import com.example.indoor.capture.ICaptureDevice.DeviceNotOpenedException;
import com.example.indoor.cv.IFilter;
import com.example.indoor.dialog.NameCreator;
import com.example.indoor.dialog.OnItemSelectListener;
import com.example.indoor.filters.CannyFilter;
import com.example.indoor.filters.ContourFilter;
import com.example.indoor.filters.GreyscaleFilter;
import com.example.indoor.filters.StockDummyFilter;
import com.example.indoor.filters.ThreadedCannyFilter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.view.ViewGroup;

public class IndoorNavi extends Activity {
	// Interface
	private static final int PREVIEW_SIZE_DIALOG_ID = 0;
	private static final int FILTER_SELECTION_DIALOG_ID = 1;
	private GLSurfaceView mGlSurfaceView;
	
	private List<Size> mAvailableCaptureSizes;
	
	private List<IFilter> mAvailableFilters;
	private TimedFilter mTimedFilter;
	private IFilter mActiveFilter;
	
	private SensorVector mGravityVector;
	private SensorVector mMagnetismVector;
	
	private IImageProcessor mImageProcessor = new MultithreadedImageProcessor( 2, 2 );
//	private IImageProcessor mImageProcessor = new SinglethreadedImageProcessor();
	private Renderer mRenderer;
	
	private SimpleTimeText mTimeText;
	private TextView mResolutionText;
	private TextView mFilterText;
	
	private ICaptureDevice mCaptureDevice;
	
	static {
		OpenCVLoader.initDebug();
	}
	
    /** Called when the activity is first created. */
    @Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		
		requestWindowFeature( Window.FEATURE_NO_TITLE );
		getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );
		
		////////////////////////////////////////////////////////////////////////
		// UI Setup
		
		setContentView( R.layout.main );
		mGlSurfaceView = (GLSurfaceView) findViewById( R.id.ui_glSurfaceView );
		mTimeText = (SimpleTimeText) findViewById( R.id.ui_timeText );
		mResolutionText = (TextView) findViewById( R.id.ui_resolutionText );
		mFilterText = (TextView) findViewById( R.id.ui_filterText );
		
		////////////////////////////////////////////////////////////////////////
		// Sensor Setup
		SensorManager manager = (SensorManager) getSystemService( Context.SENSOR_SERVICE );
		Sensor gravitySensor = manager.getDefaultSensor( Sensor.TYPE_ACCELEROMETER );
		Sensor magnetismSensor = manager.getDefaultSensor( Sensor.TYPE_MAGNETIC_FIELD );
		
		mGravityVector = new SensorVector( manager, gravitySensor, true );
		mMagnetismVector = new SensorVector( manager, magnetismSensor, true );
		
		////////////////////////////////////////////////////////////////////////
		// Capture Setup
		mCaptureDevice = new CvCaptureDevice( Highgui.CV_CAP_ANDROID );
		mAvailableCaptureSizes = mCaptureDevice.getResolutions();
		
		mAvailableFilters = createFilters();
		mActiveFilter = mAvailableFilters.get( 0 );
		mTimedFilter = new TimedFilter( mActiveFilter, mTimeText );
		mFilterText.setText( "Filter: " + mTimedFilter.getName() );
		mImageProcessor.changeFilter( mTimedFilter );
    	mImageProcessor.changeCaptureDevice( mCaptureDevice );
		
		////////////////////////////////////////////////////////////////////////
		// Rendering Setup
		
		Bitmap testBitmap = BitmapFactory.decodeResource( getResources(), R.drawable.test );
		
		mRenderer = new Renderer(
				mImageProcessor.getPreviewOutlet(),
				testBitmap,
				mGravityVector,
				mMagnetismVector );
		mGlSurfaceView.setRenderer( mRenderer );
	}
    
    @Override
    protected void onResume()
    {
    	super.onResume();
    	mGravityVector.register( SensorManager.SENSOR_DELAY_UI );
    	mMagnetismVector.register( SensorManager.SENSOR_DELAY_UI );
    	
    	//mImageProcessor.setCaptureDevice( new VideoCapture( ... ) );
    	//Size captureSize = preferences.getCaptureSize();
    	//if( captureSize != null ) {
    	//	mImageProcessor.setCaptureSize( captureSize );
    	//}
    	//else {
    	//	mImageProcessor.setBestCaptureSize( screen.width, screen.height );
    	//}

    	try {
	    	mCaptureDevice.open();
	    	mImageProcessor.changeResolution( new Size( 480.0, 320.0 ) );
	    	mResolutionText.setText( "Res: 480.0x320.0" );
	    	mImageProcessor.start();
    	}
    	catch( MissingDeviceException ex ) {
    		Log.e( "indoor", "Image Processor is missing a capture device", ex );
    	}
    	catch( DeviceNotOpenedException ex ) {
    		Log.e( "indoor", "Unable to open capture device.", ex );
    	}
    }
    
    @Override
    protected void onPause()
    {
    	super.onPause();
    	mImageProcessor.stop();
    	mCaptureDevice.close();
    	
    	//preferences.setCaptureSize( mImageProcessor.getCaptureSize );
    	
    	mGravityVector.unregister();
    	mMagnetismVector.unregister();
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = this.getMenuInflater();
    	inflater.inflate(R.menu.options, menu);
    	return true;
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	switch(item.getItemId())
    	{
    	case R.id.menu_selectCaptureSizeButton:
    		selectPreviewSize();
    		return true;
    	case R.id.menu_selectFilterButton:
    		selectFilter();
    		return true;
    	default:
        	return super.onOptionsItemSelected(item);
    	}
    }
    
    private void selectFilter() {
		showDialog(FILTER_SELECTION_DIALOG_ID);
	}
	
	public void selectPreviewSize()
	{
		showDialog(PREVIEW_SIZE_DIALOG_ID);
	}

	@Override
    public Dialog onCreateDialog(int id) {
    	Dialog dialog = null;
    	
    	switch(id) {
    	case FILTER_SELECTION_DIALOG_ID:
    		dialog = createSelectionDialog(
    				this,
    				R.string.selectFilter,
    				mAvailableFilters,
    				new NameCreator<IFilter>() {
    					public CharSequence getName( IFilter item ) {
    						return item.getName();
    					}},
    				new OnItemSelectListener<IFilter>() {
    					public void onItemSelect( IFilter item ) {
    						mTimedFilter.setFilter( item );
    						mImageProcessor.changeFilter( mTimedFilter );
    						mFilterText.setText( "Filter: " + item.getName() );
    					}}
    				);
    		break;
    	case PREVIEW_SIZE_DIALOG_ID:
    		dialog = createSelectionDialog(
    				this,
    				R.string.selectPreviewSize,
    				mAvailableCaptureSizes,
    				new NameCreator<Size>() {
    					public CharSequence getName( Size item ) {
    						return item.width + " x " + item.height;
    					}},
					new OnItemSelectListener<Size>() {
						public void onItemSelect( Size item ) {
							try {
								mImageProcessor.changeResolution( item );
								mResolutionText.setText( "Res: " + item.width + "x" + item.height );
							}
							catch( MissingDeviceException ex ) {
								Log.e( "indoor", "Image Processor is missing a capture device", ex );
							}
							catch( DeviceNotOpenedException ex ) {
								Log.e( "indoor", "Unable to set capture resolution, device is not opened.", ex );
								
							}
						}}
					);
    		break;
    	default:
    		dialog = null;
    	}
    	
		return dialog;
    }
    
    private <I> Dialog createSelectionDialog(Context context, int titleResourceId, final List<I> items,
    		final NameCreator<I> creator, final OnItemSelectListener<I> listener)
    {
    	ListAdapter adapter = new ArrayAdapter<I>(context, android.R.layout.select_dialog_item, items){
    		@Override
    		public View getView(int position, View convertView, ViewGroup parent) {
    			TextView textView = (TextView) super.getView(position, convertView, parent);
    			textView.setText(creator.getName(items.get(position)));
    			return textView;
    		}
    	};
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	builder.setTitle(titleResourceId);
    	builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(listener != null)
					listener.onItemSelect(items.get(which));
			}
		});
    	
    	return builder.create();
    }
	
	private List<IFilter> createFilters()
	{
		List<IFilter> filters = new ArrayList<IFilter>(5);
		filters.add(new StockDummyFilter());
		filters.add(new GreyscaleFilter());
		filters.add(new CannyFilter());
		filters.add(new ContourFilter());
		filters.add(new ThreadedCannyFilter());
		return filters;
	}
}
