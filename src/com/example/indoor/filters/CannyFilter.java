package com.example.indoor.filters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class CannyFilter extends GreyscaleFilter {
	private static final String FILTER_NAME = "Canny";
	protected Mat mEdges = new Mat();
	private double mThreshold1 = 80.0;
	private double mThreshold2 = 100.0;
	protected int mApertureSize = 3;
	
	@Override
	public void process(Mat input, Mat preview) {
		Imgproc.cvtColor(canny(input), preview, Imgproc.COLOR_GRAY2BGRA, 4);
	}
	
	protected Mat canny(Mat input) {
		Imgproc.Canny(convertToGrey(input), mEdges, mThreshold1, mThreshold2, mApertureSize, false);
		return mEdges;
	}
	
	public void setCannyThreshold1(double threshold) {
		mThreshold1 = threshold;
	}
	
	public void setCannyThreshold2(double threshold) {
		mThreshold2 = threshold;
	}
	
	public void setCannyApertureSize(int apertureSize) {
		mApertureSize = apertureSize;
	}

	@Override
	public String getName() {
		return FILTER_NAME;
	}

}
