package com.example.indoor.filters;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class ContourFilter extends CannyFilter {
	private static final String FILTER_NAME = "Contours";
	
	private List<MatOfPoint> mContours = new ArrayList<MatOfPoint>();
	private Mat mHierarchy = new Mat(); // ?
	private Mat mGreyscale;
	
	private int mHierarchyMode = Imgproc.RETR_LIST;
	private int mApproximationMethod = Imgproc.CHAIN_APPROX_SIMPLE;
	
	private Scalar mContourColor = new Scalar(255, 0, 255, 255);
	
	private boolean mSimplifyContours = true;

	@Override
	public void process(Mat input, Mat preview) {
		
		// Punch the image through canny and extract contours from the resulting
		// binary image.
		Imgproc.findContours(canny(input), mContours, mHierarchy, mHierarchyMode, mApproximationMethod);
		
		// Convert colored RGB input to greyscale RGB preview
		Imgproc.cvtColor(input, mGreyscale, Imgproc.COLOR_RGBA2GRAY, 1);
		Imgproc.cvtColor(mGreyscale, preview, Imgproc.COLOR_GRAY2BGRA, 4);
		
		// Draw contours
		if(mSimplifyContours) {
			MatOfPoint2f processedContour = new MatOfPoint2f();
			long numPoints;
            int buffer[] = null;
			
            for(MatOfPoint contour : mContours)
            {
            	
            	// TODO: ???
//                Imgproc.approxPolyDP(contour, processedContour, 1.0, false);
                
                numPoints = processedContour.total();
                buffer = new int[(int) (numPoints*2)]; // [x1, y1, x2, y2, ...]
                processedContour.get(0, 0, buffer);
                for(int i = 0; i < buffer.length-2; i+=2)
                {
                	float x1 = (float) buffer[i];
                	float y1 = (float) buffer[i+1];
                	float x2 = (float) buffer[i+2];
                	float y2 = (float) buffer[i+3];
//                	float dx = x2-x1, dy = y2-y1;
//                	float l = (float) Math.sqrt(dx*dx+dy*dy);
//                	float lRelative = l/mOptimalContourLength;
                	Core.line(preview, new Point(x1,y1), new Point(x2,y2), mContourColor);
                }
            }
		} else {
			Imgproc.drawContours(preview, mContours, -1, mContourColor);
		}
	}
	
	public void setHierarchyMode(int hierarchyMode) {
		mHierarchyMode = hierarchyMode;
	}
	
	public void setApproximationMethod(int approximationMethod) {
		mApproximationMethod = approximationMethod;
	}

	@Override
	public void cleanup() {
		super.cleanup();
	}

	@Override
	public String getName() {
		return FILTER_NAME;
	}

}
