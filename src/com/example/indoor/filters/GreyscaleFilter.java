package com.example.indoor.filters;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import com.example.indoor.cv.IFilter;

public class GreyscaleFilter implements IFilter {
	private static final String FILTER_NAME = "Convert to Greyscale";
	private Mat mIntermediate;
	private Size mSize;

	public GreyscaleFilter() {
	}
	
	@Override
	public synchronized void resize( Size size ) {
		if( mIntermediate == null ) {
			mIntermediate = new Mat();
		}
		mIntermediate.create( size, CvType.CV_8U );
		mSize = size;
	}
	
	@Override
	public synchronized Size getPreviewSize() {
		return  mSize;
	}
	
	@Override
	public synchronized void process(Mat input, Mat preview) {
		Imgproc.cvtColor(convertToGrey(input), preview, Imgproc.COLOR_GRAY2BGRA, 4);
	}
	
	protected Mat convertToGrey(Mat input) {
		Imgproc.cvtColor(input, mIntermediate, Imgproc.COLOR_RGBA2GRAY, 1);
		return mIntermediate;
	}
	
	public synchronized void cleanup() {
		if( mIntermediate != null ) {
			mIntermediate.release();
			mIntermediate = null;
		}
	}

	@Override
	public String getName() {
		return FILTER_NAME;
	}
}
