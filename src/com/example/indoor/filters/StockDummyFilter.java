package com.example.indoor.filters;

import org.opencv.core.Mat;
import org.opencv.core.Size;

import com.example.indoor.cv.IFilter;

/** A stock dummy filter that does nothing but forward the input image, derp. */
public class StockDummyFilter implements IFilter {
	private static final String FILTER_NAME = "Stock Dummy Filter";
	
	private Size mSize = new Size( 0, 0 ); 

	@Override
	public void resize( Size size ) {
		mSize = size;
	}
	
	@Override
	public Size getPreviewSize() {
		return mSize;
	}
	
	public void cleanup() {
	}
	
	@Override
	public void process(Mat input, Mat preview) {
		input.copyTo(preview);
	}

	@Override
	public String getName() {
		return FILTER_NAME;
	}

}
