package com.example.indoor.filters;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;

import android.util.Log;

import com.example.indoor.cv.IFilter;

public class ThreadedCannyFilter implements IFilter {
	private int mApertureSize = 3; // TODO: inherit to left & right filters
	
	private CannyFilter mLeftCannyFilter = new CannyFilter();
	private CannyFilter mRightCannyFilter = new CannyFilter();
	
	private Rect mLeftInputRegion  = null;
	private Rect mRightInputRegion = new Rect();
	
	private Mat mLeftPreview  = new Mat();
	private Mat mRightPreview = new Mat();
	
	private Rect mLeftPreviewRegion  = new Rect();
	private Rect mRightPreviewRegion = new Rect();
	
	private Rect mLeftOutputRegion  = new Rect();
	private Rect mRightOutputRegion = new Rect();
	
	private Size mSize;
	
	private class Worker extends Thread {
		
		private IFilter mFilter;
		private Mat mInput, mPreview;
		
		public Worker( IFilter filter, Mat input, Mat preview ) {
			mFilter = filter;
			mInput = input;
			mPreview = preview;
		}
		@Override
		public void run() {
			mFilter.process( mInput, mPreview );
		}
	}
	
	public ThreadedCannyFilter() {
	}
	
	@Override
	public void resize( Size size ) {
		if( size.width == 0 ) {
			return;
		}
		
		if( mLeftPreview == null || mRightPreview == null ) {
			mLeftPreview = new Mat();
			mRightPreview = new Mat();
		}
		
		int width = (int)size.width;
		int height = (int)size.height;
		int split = width / 2;
		int kHalf = mApertureSize / 2;
		
		int leftInputWidth = split + kHalf;
		int rightInputWidth = width - split + kHalf;
		
		Size leftInputSize  = new Size( leftInputWidth, height );
		Size rightInputSize = new Size( rightInputWidth, height );
		
		mLeftCannyFilter.resize( leftInputSize );
		mRightCannyFilter.resize( rightInputSize );
		
		mLeftInputRegion  = new Rect( 0, 0, leftInputWidth, height );
		mRightInputRegion = new Rect( split-kHalf, 0, rightInputWidth, height );
		
		mLeftPreview.create( leftInputWidth, height, CvType.CV_8U );
		mRightPreview.create( rightInputWidth, height, CvType.CV_8U );
		
		mLeftPreviewRegion  = new Rect( 0, 0, split, height );
		mRightPreviewRegion = new Rect( kHalf, 0, width-split, height );
		
		mLeftOutputRegion  = new Rect( 0, 0, split, height );
		mRightOutputRegion = new Rect( split, 0, width-split, height );
	}
	
	@Override
	public void process( Mat input, Mat preview ) {
		Mat leftInput  = input.submat( mLeftInputRegion );
		Mat rightInput = input.submat( mRightInputRegion );
		
//		mLeftCannyFilter.process( leftInput, mLeftPreview );
//		mRightCannyFilter.process( rightInput, mRightPreview );
		
		Worker left  = new Worker( mLeftCannyFilter, leftInput, mLeftPreview );
		Worker right = new Worker( mRightCannyFilter, rightInput, mRightPreview );
		left.start();
		right.start();
		
		try {
			left.join();
			right.join();
		}
		catch( InterruptedException ex ) {
			Log.e( "indoor.ThreadedCannyFilter", "interrupted during thread join", ex );
		}
		
		Mat leftPreview  = mLeftPreview.submat( mLeftPreviewRegion );
		Mat rightPreview = mRightPreview.submat( mRightPreviewRegion );
		
		Mat leftOutput  = preview.submat( mLeftOutputRegion );
		Mat rightOutput = preview.submat( mRightOutputRegion );
		
		leftPreview.copyTo( leftOutput );
		rightPreview.copyTo( rightOutput );
	}
	
	@Override
	public String getName() {
		return "Canny (Split Image)";
	}

	@Override
	public void cleanup() {
		mLeftPreview.release();
		mRightPreview.release();
		
		mLeftPreview = null;
		mRightPreview = null;
	}

	@Override
	public Size getPreviewSize() {
		return mSize;
	}
}
