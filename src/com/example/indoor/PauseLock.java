package com.example.indoor;

public class PauseLock {
	public Boolean mPause = false;
	private Boolean mIsPaused = false;
	
	public PauseLock( boolean startAsPaused ) {
		mIsPaused = startAsPaused;
	}
	
	public void activate() throws InterruptedException {
		synchronized( mPause ) {
			mPause = true;
		}
			
		synchronized( mIsPaused ) {
			while( !mIsPaused ) {
				mIsPaused.wait();
			}
		}
	}
	
	public void check() throws InterruptedException {
		synchronized( mPause ) {
			if( mPause ) {
				synchronized (mIsPaused) {
					mIsPaused = true;
					mIsPaused.notifyAll();
				}
		
				while( mPause ) {
					mPause.wait();
				}
			}
		}
		
		mIsPaused = false;
	}
	
	public void deactivate() {
		synchronized (mPause) {
			mPause = false;
			mPause.notifyAll();
		}
	}
}