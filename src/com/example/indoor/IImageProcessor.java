package com.example.indoor;

import org.opencv.core.Mat;
import org.opencv.core.Size;

import com.example.indoor.capture.ICaptureDevice;
import com.example.indoor.capture.CaptureWorker.MissingDeviceException;
import com.example.indoor.capture.ICaptureDevice.DeviceNotOpenedException;
import com.example.indoor.cv.IFilter;
import com.example.indoor.resources.IOutlet;

public interface IImageProcessor {

	/**
	 * Replaces the current filter with the argument.
	 * 
	 * Should ensure a (thread) state in which the filter can safely be replaced
	 * first. May call {@link IFilter#cleanup()} on the former and
	 * {@link IFilter#resize} plus {@link IFilter#getPreviewSize()} on the
	 * latter.
	 * 
	 * @param filter the filter to replace the current one
	 */
	public abstract void changeFilter( IFilter filter );

	/**
	 * Replaces the current capture device with the argument.
	 * 
	 * Should ensure a (thread) state in which the device can safely be replaced
	 * first. Does not involve opening or closing any of the devices, this
	 * should be the responsibility of the caller.
	 * 
	 * @param captureDevice the capture device to replace the current one
	 */
	public abstract void changeCaptureDevice( ICaptureDevice captureDevice );

	/**
	 * Changes the current capture resolution.
	 * 
	 * Should ensure a (thread) state in which the resolution can safely be set
	 * first. A capture device should be set and opened beforehand via
	 * {@link #changeCaptureDevice} and {@link ICaptureDevice#open}. Valid
	 * resolutions can be retrieved from the capture device, the behavior on
	 * other arguments may not be defined.
	 * 
	 * @param resolution the desired resolution of frames captured from the current
	 *             device
	 *             
	 * @throws MissingDeviceException   if no capture device has been set before
	 * @throws DeviceNotOpenedException if the current capture device is not
	 *                                  opened beforehand
	 * @throws IllegalArgumentException if null is passed as the size argument
	 * 
	 * @see #changeCaptureDevice
	 * @see ICaptureDevice#getResolutions
	 * @see ICaptureDevice#open
	 */
	public abstract void changeResolution( Size resolution )
			throws MissingDeviceException, DeviceNotOpenedException,
			IllegalArgumentException;

	public abstract void start() throws MissingDeviceException;

	public abstract void stop();

	public abstract IOutlet<Mat> getPreviewOutlet();

}