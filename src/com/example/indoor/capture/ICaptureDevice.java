package com.example.indoor.capture;

import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Size;

/**
 * A source of video streams.
 * @author Markus Dittmann
 *
 */
public interface ICaptureDevice {
	/**
	 * Thrown when the device needs to be opened before calling a method.
	 * Relevant methods are marked with a `throws` declaration.
	 */
	public class DeviceNotOpenedException extends Exception {
		private static final long serialVersionUID = 3969015163103706444L;
	};
	
	/**
	 * Returns all resolutions supported by the source.
	 * This is the only method available without opening the device.
	 * 
	 * @return A list of available resolutions.
	 * @see    #setResolution(Size)
	 * @see    #getCurrentResolution()
	 */
	public List<Size> getResolutions();
	
	/**
	 * Sets the resolution for future captured images. Sizes provided by
	 * {@link #getResolutions()} are applicable. Behavior for other sizes is
	 * undefined.
	 * @param  size the size of the captured images
	 * @throws DeviceNotOpenedException if the device is not opened before
	 *         the method call
	 * @see    #getResolutions()
	 * @see    #getCurrentResolution()
	 */
	public void setResolution( Size size ) throws DeviceNotOpenedException;
	
	/**
	 * Returns the capture resolution currently set for the device with
	 * {@link #setResolution(Size)}.
	 * @return the currently set capture resolution
	 * @throws DeviceNotOpenedException if the device is not opened before
	 *         the method call
	 * @see    #getResolutions()
	 * @see    #setResolution(Size)
	 */
	public Size getCurrentResolution() throws DeviceNotOpenedException;
	
	/**
	 * Opens the device to set/get the capture resolution and retrieve images.
	 * @return {@code true} on success, {@code false} on failure
	 * @see    #close()
	 * @see    #setResolution(Size)
	 * @see    #getCurrentResolution()
	 */
	public boolean open();
	
	/**
	 * Grabs an image for later retrieval.
	 * @return {@code true} on success
	 *         {@code false} on failure (e.g. no more frames in video file)
	 * @throws DeviceNotOpenedException if the device is not opened before
	 *                                  the method call
	 * @see    #retrieve(Mat)
	 */
	public boolean grab() throws DeviceNotOpenedException;
	
	/**
	 * Retrieves a previously grabbed image and stores it in {@code image}.
	 * @param  image the matrix to store the image. Internal memory may be
	 *               overwritten.
	 * @throws DeviceNotOpenedException
	 *               if the device is not opened before
	 *               the method call
	 * @see          #grab()
	 */
	public void retrieve( Mat image ) throws DeviceNotOpenedException;
	
	/**
	 * Closes the capture device.
	 * @see #open()
	 */
	public void close();
	
	/**
	 * Checks whether the device is currently opened.
	 * @return {@code true} if the device is opened,
	 *         {@code false} otherwise
	 */
	public boolean isOpened();
}
