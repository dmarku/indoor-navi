package com.example.indoor.capture;

import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;


/**
 * Adapter to the {@link ICaptureDevice} interface for an OpenCV {@code VideoCapture} device
 * 
 * @author Markus Dittmann
 * @see    org.opencv.highgui.VideoCapture
 */
public class CvCaptureDevice implements ICaptureDevice {
	
	private int mDeviceId;
	private VideoCapture mDevice = null;
	private List<Size> mResolutions = null;
	
	private static final DeviceNotOpenedException mException = new DeviceNotOpenedException(); 
	
	/**
	 * Creates an encapsulated OpenCV {@code VideoCapture} device.
	 * @param deviceId the OpenCV device ID to open the device for
	 * @see            org.opencv.highgui.VideoCapture#VideoCapture(int)
	 */
	public CvCaptureDevice( int deviceId ) {
		mDeviceId = deviceId;
		mDevice = new VideoCapture( mDeviceId );
		mResolutions = mDevice.getSupportedPreviewSizes();
		mDevice.release();
	}
	
	@Override
	public List<Size> getResolutions() {
		return mResolutions;
	}
	
	@Override
	public void setResolution( Size size ) throws DeviceNotOpenedException {
		if( !mDevice.isOpened() ) {
			throw mException;
		}
		else {
			mDevice.set( Highgui.CV_CAP_PROP_FRAME_WIDTH, size.width );
			mDevice.set( Highgui.CV_CAP_PROP_FRAME_HEIGHT, size.height );
		}
	}
	
	@Override
	public Size getCurrentResolution() throws DeviceNotOpenedException {
		if( !mDevice.isOpened() ) {
			throw mException;
		}
		else {
			double width = mDevice.get( Highgui.CV_CAP_PROP_FRAME_WIDTH );
			double height = mDevice.get( Highgui.CV_CAP_PROP_FRAME_HEIGHT );
			return new Size( width, height );
		}
	}
	
	@Override
	public boolean open() {
		return mDevice.open( mDeviceId );
	}
	
	@Override
	public boolean grab() throws DeviceNotOpenedException {
		if( !mDevice.isOpened() ) {
			throw mException;
		}
		else {
			return mDevice.grab();
		}
	}
	
	@Override
	public void retrieve( Mat image ) throws DeviceNotOpenedException {
		if( !mDevice.isOpened() ) {
			throw mException;
		}
		else {
			mDevice.retrieve(image, Highgui.CV_CAP_ANDROID_COLOR_FRAME_RGBA);
		}
	}
	
	@Override
	public void close() {
		mDevice.release();
	}
	
	@Override
	public boolean isOpened() {
		return mDevice.isOpened();
	}
}
