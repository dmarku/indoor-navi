package com.example.indoor.capture;

import org.opencv.core.Mat;

import com.example.indoor.PauseLock;
import com.example.indoor.capture.ICaptureDevice.DeviceNotOpenedException;
import com.example.indoor.resources.IOutlet;
import com.example.indoor.resources.IResource;

import android.util.Log;

/**
 * fills a BlockingQueue with captured images between calls to startCapture()
 * and stopCapture() until its capacity is reached.
 */
public class CaptureWorker implements Runnable
{
	
	public class MissingDeviceException extends Exception {
		private static final long serialVersionUID = -6525107413564183737L;

		public MissingDeviceException() {
			super( "Unable to start capture thread: no capture device has been set." );
		}
	}
	
	private static final String TAG = "indoor.CaptureWorker";
	
	private ICaptureDevice mCaptureDevice = null;
	private Mat mCapturedImage = null;
	private IOutlet<Mat> mOutput;
	
	private Thread mWorkerThread = null;
	private PauseLock mPauseLock = new PauseLock( true );
	
	public CaptureWorker( ICaptureDevice device, IOutlet<Mat> output ) {
		mCaptureDevice = device;
		mOutput = output;
	}
	
	public void setCaptureDevice( ICaptureDevice device ) {
		mCaptureDevice = device;
	}
	
	public void pause() throws InterruptedException {
		mPauseLock.activate();
	}
	
	public void resume() {
		mPauseLock.deactivate();
	}
	
	public void startCapture() throws MissingDeviceException
	{
		if( mCaptureDevice == null ) {
			throw new MissingDeviceException();
		}
		
		if( mCaptureDevice.isOpened() ) {
			mWorkerThread = new Thread( this, "Capture Thread" );
			mPauseLock.deactivate();
			mWorkerThread.start();
		}
		else {
			Log.e( TAG, "Unable to start capturing: capture device is closed." );
		}
	}
	
	public void stopCapture()
	{
		Log.d(TAG, "stopped capture");
		if(mWorkerThread != null)
		{
			mWorkerThread.interrupt();
			mWorkerThread = null;
		}
	}

	@Override
	public void run() {
		IResource<Mat> output = null;
		try {
			while( true ) {
				mPauseLock.check();
				
				if( mCaptureDevice.grab() ) {
					mCapturedImage = new Mat();
					mCaptureDevice.retrieve( mCapturedImage );
					output = mOutput.take();
					mCapturedImage.copyTo( output.getContents() );
					output.release();
					output = null;
					
					Log.v( TAG, "Captured a frame, ready for processing" );
				}
				else {
					Log.e( TAG, "UNABLE TO RETRIEVE IMAGE FROM CAMERA, WAAAAAAARGH!!!!!!!!" );
					return;
				}
			}
		}
		catch( InterruptedException ex ) {
			Log.e( TAG, "InterruptedException received :(", ex );
			// safely release allocated resource in case _someone_ would shut
			// the thread down the hard way while a resource is allocated.
			if( output != null ) {
				output.release();
				output = null;
			}
		}
		catch( DeviceNotOpenedException ex ) {
			Log.e( TAG, "Capture thread aborted, device is unexpectedly closed.", ex );
		}
	}
}
