package com.example.indoor.cv;

import org.opencv.core.Mat;
import org.opencv.core.Size;

/** Basic interface for image filtering */
public interface IFilter {
	
	/** Notifies the filter about a change in size of the image that is passed as input. */
	public void resize( Size size );
	
	/**
	 * Notifies the filter that it will not be used in the foreseeable future.
	 * Use this to free internally used resources (e.g. intermediate images).
	 */
	public void cleanup();
	
	/** Queries the filter which size is expected for preview images. */
	public Size getPreviewSize();
	
	/**
	 * Receives an image, returns a processed image.
	 */
	public void process(final Mat input, Mat preview);
	
	/** Returns a descriptive name of what this specific filter is supposed to do. */
	public String getName();
}
