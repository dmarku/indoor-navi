package com.example.indoor.cv;

import org.opencv.core.Mat;
import com.example.indoor.PauseLock;
import com.example.indoor.resources.IOutlet;
import com.example.indoor.resources.IResource;

import android.util.Log;

/**
 * processes images from the supplied input queue and stores previews in the
 * supplied output queue.
 */
public class ProcessingWorker implements Runnable
{
	private static final String TAG = "indoor.ProcessingWorker";

	private IOutlet<Mat> mInput;
	private IOutlet<Mat> mOutput;
	private IFilter mFilter = null;
	
	private Thread mProcessingThread;
	private PauseLock mPauseLock = new PauseLock( false );
	
	public ProcessingWorker( IOutlet<Mat> input, IOutlet<Mat> output ) {
		mInput = input;
		mOutput = output;
		mProcessingThread = new Thread( this, "Processing Thread" );
	}
	
	public void pause() throws InterruptedException {
		mPauseLock.activate();
	}
	
	public void resume() {
		mPauseLock.deactivate();
	}
	
	public void startProcessing()
	{
		if( mProcessingThread == null ) {
			mProcessingThread = new Thread( this, "Processing Thread" );
		}
		
		if(!mProcessingThread.isAlive()) {
			mPauseLock.deactivate();
			mProcessingThread.start();
		}
	}
	
	public void stopProcessing()
	{
		if( mProcessingThread != null ) {
			if( mProcessingThread.isAlive() ) {
				mProcessingThread.interrupt();
			}
			mProcessingThread = null;
		}
	}

	@Override
	public void run() {
		IResource<Mat> input = null;
		IResource<Mat> output = null;
		try {
			while(true)
			{
				mPauseLock.check();

				input = mInput.take();
				Log.v("indoor.ProcessingWorker", "Acquired input, waiting for output..." );
				output = mOutput.take();
				Log.v("indoor.ProcessingWorker", "Acquired output, processing..." );
				
				if( mFilter != null ) {
					mFilter.process( input.getContents(), output.getContents() );
				}
				else {
					input.getContents().copyTo( output.getContents() );
				}
				Log.v("indoor.ProcessingWorker", "Processed a frame, ready for display." );
				
				input.release();
				input = null;
				output.release();
				output = null;
				Log.v("indoor.ProcessingWorker", "Resources released." );
			}
		}
		catch( InterruptedException ex )
		{
			Log.e(TAG, "InterruptedException received :(");
			if( input != null ) {
				input.release();
				input = null;
			}
			if( output != null ) {
				output.release();
				output = null;
			}
		}
	}
	
	public void setFilter( IFilter filter ) {
		mFilter = filter;
	}
}
