package com.example.indoor;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;

import android.util.Log;

import com.example.indoor.capture.CaptureWorker;
import com.example.indoor.capture.ICaptureDevice;
import com.example.indoor.capture.CaptureWorker.MissingDeviceException;
import com.example.indoor.capture.ICaptureDevice.DeviceNotOpenedException;
import com.example.indoor.cv.IFilter;
import com.example.indoor.cv.ProcessingWorker;
import com.example.indoor.resources.IImagePool;
import com.example.indoor.resources.IOutlet;
import com.example.indoor.resources.queues.ImageQueues;

/**
 * A complete multi-threaded Processing Pipeline from Video Capture to Preview
 * Output.
 * @author Markus Dittmann
 *
 */
public class MultithreadedImageProcessor implements IImageProcessor {
	private static final String TAG = "indoor.ImageProcessor";
	
	private IImagePool mCaptureImages;
	private IImagePool mPreviewImages;
	
	private CaptureWorker mCaptureWorker;
	private ProcessingWorker mProcessingWorker;
	
	private Size mCaptureSize = null;
	private Size mPreviewSize = null;
	private ICaptureDevice mCaptureDevice = null;
	private IFilter mFilter = null;
	
	private boolean mIsStarted = false;
	
	public MultithreadedImageProcessor( int captureCapacity, int previewCapacity ) {
//    	mCaptureImages = new ImageRing( captureCapacity, CvType.CV_8UC4 );
//    	mPreviewImages = new ImageRing( previewCapacity, CvType.CV_8UC4 );
    	mCaptureImages = new ImageQueues( captureCapacity, CvType.CV_8UC4 );
    	mPreviewImages = new ImageQueues( previewCapacity, CvType.CV_8UC4 );
    	
    	mCaptureWorker = new CaptureWorker( null, mCaptureImages.getWriteOutlet() );
    	mProcessingWorker = new ProcessingWorker( mCaptureImages.getReadOutlet(), mPreviewImages.getWriteOutlet() );
	}
	
	@Override
	public void changeFilter( IFilter filter ) {
		
		if( mIsStarted ) {
			// FIXME: softly suspend worker thread instead of interrupting
//			try {
//				mProcessingWorker.pause();
				mProcessingWorker.stopProcessing();
				setFilter( filter );
				mProcessingWorker.startProcessing();
				// FIXME: softly suspend worker thread instead of interrupting
//				mProcessingWorker.resume();
//			}
//			catch( InterruptedException ex ) {
//				Log.e( TAG, "Unable to change filter: interrupted while pausing worker", ex );
//			}
		}
		else {
			setFilter( filter );
		}
	}
	
	/**
	 * Directly replaces the current filter without regards to thread states etc.
	 * For internal use only!
	 * 
	 * @param filter the filter to replace the current one
	 */
	private void setFilter( IFilter filter ) {
		
		if( mFilter != null ) {
			mFilter.cleanup();
		}
		
		if( mCaptureSize != null ) {
			if( filter != null ) {
				filter.resize( mCaptureSize );
				mPreviewSize = filter.getPreviewSize();
			}
			else {
				mPreviewSize = mCaptureSize;
			}
			mPreviewImages.resize( mPreviewSize );
		}
		
		mFilter = filter;
		mProcessingWorker.setFilter( filter );
		Log.d( TAG, "Filter set to " + mFilter.getName() );
	}
	
	@Override
	public void changeCaptureDevice( ICaptureDevice captureDevice ) {
		if( mIsStarted ) {
			try {
				mCaptureWorker.pause();
				setCaptureDevice( captureDevice );
				mCaptureWorker.resume();
			}
			catch( InterruptedException ex ) {
				Log.e( TAG, "Unable to change capture device: interrupted while pausing worker", ex );
			}
		}
		else {
			setCaptureDevice( captureDevice );
		}
	}
	
	/**
	 * Directly replaces the current capture device without regarding thread
	 * state etc.
	 * For internal use only!
	 * 
	 * @param captureDevice the capture device to replace the current one
	 */
	private void setCaptureDevice( ICaptureDevice captureDevice ) {
		mCaptureDevice = captureDevice;
		mCaptureWorker.setCaptureDevice( captureDevice );
	}
	
	@Override
	public void changeResolution( Size resolution )
			throws MissingDeviceException, DeviceNotOpenedException, IllegalArgumentException {
		
		if( resolution == null ) {
			throw new IllegalArgumentException( "size must NOT be null" );
		}
		
		if( mIsStarted ) {
			// FIXME: softly suspend worker thread instead of interrupting
//			try {
//				mCaptureWorker.pause();
//				mProcessingWorker.pause();
				mCaptureWorker.stopCapture();
				mProcessingWorker.stopProcessing();
				setResolution( resolution );
				mProcessingWorker.startProcessing();
				mCaptureWorker.startCapture();

				// FIXME: softly suspend worker thread instead of interrupting
//				mProcessingWorker.resume();
//				mCaptureWorker.resume();
//			}
//			catch( InterruptedException ex ) {
//				Log.e( TAG, "Could not set capture resolution: interrupted while pausing worker threads", ex );
//			}
		}
		else {
			setResolution( resolution );
		}
	}
	
	private void setResolution( Size resolution )
			throws DeviceNotOpenedException {

		mCaptureDevice.setResolution( resolution );
		mCaptureSize = mCaptureDevice.getCurrentResolution();
		
		mCaptureImages.resize( mCaptureSize );
		
		if( mFilter != null ) {
			mFilter.resize( mCaptureSize );
			mPreviewSize = mFilter.getPreviewSize();
		}
		else {
			mPreviewSize = mCaptureSize;
		}
		
		mPreviewImages.resize( mPreviewSize );
		
	}
	
	@Override
	public void start() throws MissingDeviceException {
		mIsStarted = true;
    	mProcessingWorker.startProcessing();
    	mCaptureWorker.startCapture();
	}
	
	@Override
	public void stop() {
    	mCaptureWorker.stopCapture();
    	mProcessingWorker.stopProcessing();
		mIsStarted = false;
	}
	
	@Override
	public IOutlet<Mat> getPreviewOutlet() {
		return mPreviewImages.getReadOutlet();
	}
}
