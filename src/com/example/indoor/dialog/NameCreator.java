package com.example.indoor.dialog;

public interface NameCreator<I> {
	public abstract CharSequence getName(I item);
}
