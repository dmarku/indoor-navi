package com.example.indoor.dialog;

public interface OnItemSelectListener<I> {
	public abstract void onItemSelect(I item);
}
