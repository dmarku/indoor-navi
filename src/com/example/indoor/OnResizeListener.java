package com.example.indoor;

public interface OnResizeListener {
	public void onResize(int width, int height);
}
