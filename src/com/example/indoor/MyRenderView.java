package com.example.indoor;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.Color;
import com.example.gl.Drawable;
import com.example.gl.Geometry;
import com.example.gl.drawable.GeometryDrawable;
import com.example.gl.transform.OrthoProjection;
import com.example.gl.view.ProjectionView;

public class MyRenderView extends ProjectionView {
	private OrthoProjection mProjection = new OrthoProjection(-1, 1, -1, 1, -1, 1);
	private Geometry mGeometry = new Geometry();
	private GeometryDrawable mGeometryDrawable = new GeometryDrawable(mGeometry);
	private Drawable mRootDrawable = null;
	
	public MyRenderView() {
		setProjection(mProjection);
		
		float size = 2.0f;
		float halfSize = 0.5f*size;
		mGeometry.setVertexCoords(new float[] {
				-halfSize, -halfSize, 0.0f, 1.0f,
				 halfSize, -halfSize, 0.0f, 1.0f,
				 halfSize,  halfSize, 0.0f, 1.0f,
				 -halfSize, halfSize, 0.0f, 1.0f
		});
		mGeometry.setNormalCoords(new float[] {
				0.0f, 0.0f, 1.0f,
				0.0f, 0.0f, 1.0f,
				0.0f, 0.0f, 1.0f,
				0.0f, 0.0f, 1.0f
		});
		mGeometry.setIndices(new short[] {
				0,1,2,3
		});
		
		mGeometryDrawable.setPrimitiveMode(GL10.GL_LINE_LOOP);
		mGeometryDrawable.setColor(Color.GREEN);
		mGeometryDrawable.useColor(true);
	}
	
	public void setRootDrawable(Drawable rootDrawable) {
		mRootDrawable = rootDrawable;
	}
	
	@Override
	public void resize(float width, float height) {
		float aspect = width/height;
		mProjection.setWidthBoundaries(-aspect, aspect);
	}

	@Override
	public void onDraw(GL10 gl) {
		gl.glPushMatrix();
//		gl.glScalef(0.8f, 0.8f, 0.8f);
		mGeometryDrawable.draw(gl);
		if(mRootDrawable != null) {
			mRootDrawable.draw(gl);
		}
		gl.glPopMatrix();
	}

}
