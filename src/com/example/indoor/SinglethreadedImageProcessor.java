package com.example.indoor;

import org.opencv.core.Mat;
import org.opencv.core.Size;

import com.example.indoor.capture.CaptureWorker.MissingDeviceException;
import com.example.indoor.capture.ICaptureDevice;
import com.example.indoor.capture.ICaptureDevice.DeviceNotOpenedException;
import com.example.indoor.cv.IFilter;
import com.example.indoor.resources.IOutlet;

public class SinglethreadedImageProcessor implements IImageProcessor, Runnable {

	private ICaptureDevice mDevice = null;
	private Size mCaptureSize = null;
	
	private Mat mImage = new Mat();
	
	private IFilter mFilter = null;
	private Size mPreviewSize = null;
	
	private Mat mPreview = new Mat();
	
	private Thread mThread = null;
	
	private ICaptureDevice mNewDevice = null;
	private boolean mChangeDevice = false;
	
	private IFilter mNewFilter = null;
	private boolean mChangeFilter = false;
	
	private Size mNewSize = null;
	private boolean mChangeSize = false;
	
	public SinglethreadedImageProcessor() {
		mThread = new Thread( this );
	}
	
	@Override
	public void changeFilter( IFilter filter ) {
		mNewFilter = filter;
		mChangeFilter = true;
	}

	@Override
	public void changeCaptureDevice( ICaptureDevice captureDevice ) {
		mNewDevice = captureDevice;
		mChangeDevice = true;
	}

	@Override
	public void changeResolution( Size size )
			throws MissingDeviceException,
			DeviceNotOpenedException, IllegalArgumentException {
		
		if( size == null ) {
			throw new IllegalArgumentException("size must NOT be null" );
		}
	}

	@Override
	public void start() throws MissingDeviceException {
		// TODO Auto-generated method stub

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

	@Override
	public IOutlet<Mat> getPreviewOutlet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run() {
		while( true ) {
			
			if( mNewDevice != null ) {
				
			}
			
		}
	}

}
