package com.example.indoor.resources;

public interface IFactory<T> {
	T create();
}
