package com.example.indoor.resources;

/**
 * @brief Defines an endpoint to acquire managed resources.
 * This interface provides methods to acquire a resource in blocking and
 * non-blocking manners.
 * 
 * @author Markus Dittmann
 *
 * @param <T> The type of managed content.
 */

public interface IOutlet<T> {
	IResource<T> take() throws InterruptedException;
	IResource<T> poll();
}
