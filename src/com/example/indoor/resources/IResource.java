package com.example.indoor.resources;

/**
 * @brief A successfully acquired resource.
 * The interface provides access to the content of the resource and means
 * to signal that the resource is not used anymore.
 * 
 * @author Markus Dittmann
 *
 * @param <T> The type of content managed by the resource.
 */

public interface IResource<T> {
	T getContents();
	void release();
}
