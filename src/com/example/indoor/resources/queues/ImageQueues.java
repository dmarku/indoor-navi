package com.example.indoor.resources.queues;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.opencv.core.Mat;
import org.opencv.core.Size;

import android.util.Log;

import com.example.indoor.resources.IImagePool;
import com.example.indoor.resources.IOutlet;
import com.example.indoor.resources.IResource;

public class ImageQueues implements IImagePool {
	
	BlockingQueue<Mat> mAvailableQueue = null;
	BlockingQueue<Mat> mFilledQueue = null;
	
	IOutlet<Mat> mWriteOutlet = null;
	IOutlet<Mat> mReadOutlet = null;
	
	private int mType = -1;
	
	private class Resource implements IResource<Mat> {

		private Mat mImage = null;
		private BlockingQueue<Mat> mDrain;
		
		public Resource( Mat image, BlockingQueue<Mat> drain ) {
			mImage = image;
			mDrain = drain;
		}
		
		@Override
		public Mat getContents() {
			return mImage;
		}

		@Override
		public void release() {
			try {
				mDrain.put(mImage);
			}
			catch( InterruptedException ex ) {
				Log.e( "indoor.ImageRing2", "Interrupted during image insert.", ex );
			}
		}	
	}
	
	private class Outlet implements IOutlet<Mat> {

		private BlockingQueue<Mat> mSource, mDrain;
		
		public Outlet( BlockingQueue<Mat> source, BlockingQueue<Mat> drain ) {
			mSource = source;
			mDrain = drain;
		}
		
		@Override
		public IResource<Mat> take() throws InterruptedException {
			return new Resource( mSource.take(), mDrain );
		}

		@Override
		public IResource<Mat> poll() {
			Mat image = mSource.poll();
			if( image == null ) {
				return null;
			}
			else {
				return new Resource( image, mDrain );
			}
		}
	}
	
	public ImageQueues( int capacity, int type ) {
		mAvailableQueue = new ArrayBlockingQueue<Mat>( capacity );
		mFilledQueue = new ArrayBlockingQueue<Mat>( capacity );
		
		mWriteOutlet = new Outlet( mAvailableQueue, mFilledQueue );
		mReadOutlet = new Outlet( mFilledQueue, mAvailableQueue );
		
		mType = type;
		
		for( int i = 0; i < capacity; ++i ) {
			mAvailableQueue.add( new Mat() );
		}
	}
	
	public void resize( Size size ) {
		for( Mat m : mAvailableQueue ) {
			m.create( size, mType );
		}
		
		for( Mat m : mFilledQueue ) {
			m.create( size, mType );
			mAvailableQueue.add( m );
		}
		
		mFilledQueue.clear();
		
	}

	@Override
	public IOutlet<Mat> getWriteOutlet() {
		return mWriteOutlet;
	}

	@Override
	public IOutlet<Mat> getReadOutlet() {
		return mReadOutlet;
	}

}
