package com.example.indoor.resources;

import org.opencv.core.Mat;
import org.opencv.core.Size;

public interface IImagePool extends IReadWritePool<Mat> {
	public void resize( Size size );
}
