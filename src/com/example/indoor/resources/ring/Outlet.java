package com.example.indoor.resources.ring;

import com.example.indoor.resources.IOutlet;

public class Outlet<T> implements IOutlet<T> {

	Resource<T> mResource;

	Resource.State mRemoveState;
	Resource.State mAcquiredState;
	Resource.State mReleasedState;
	
	public Outlet(
			Resource<T> resource,
			Resource.State removeState,
			Resource.State acquiredState,
			Resource.State releasedState ) {
		
		mResource = resource;
		
		mRemoveState = removeState;
		mAcquiredState = acquiredState;
		mReleasedState = releasedState;
		
	}

	@Override
	public Resource<T> take() throws InterruptedException {
		mResource.waitForState( mRemoveState );
		return prepareResource();
	}

	@Override
	public Resource<T> poll() {
		if( mResource.hasState( mRemoveState ) ) {
			return prepareResource();
		} else {
			return null;
		}
	}
	
	Resource<T> prepareResource() {
		Resource<T> preparedResource = mResource;
		preparedResource.mOutlet = this;
		preparedResource.setState( mAcquiredState );
		
		mResource = mResource.mNext;
		
		return preparedResource;
	}
}
