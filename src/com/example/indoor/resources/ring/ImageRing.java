package com.example.indoor.resources.ring;

import org.opencv.core.Mat;
import org.opencv.core.Size;

import com.example.indoor.resources.IFactory;
import com.example.indoor.resources.IImagePool;
import com.example.indoor.resources.IResource;

public class ImageRing extends ReadWriteRing<Mat> implements IImagePool {

	private int mType = -1;

	public ImageRing( int capacity, int type ) {
		super( capacity, new IFactory<Mat>() {
			public Mat create() { return new Mat(); }
		} );
		
		mType = type;
	}
	
	public void resize( Size size ) {
		for( IResource<Mat> resource : this ) {
			resource.getContents().create( size, mType );
		}
	}
}
