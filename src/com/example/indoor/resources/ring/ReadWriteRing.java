package com.example.indoor.resources.ring;

import java.util.NoSuchElementException;

import com.example.indoor.resources.IFactory;
import com.example.indoor.resources.IOutlet;
import com.example.indoor.resources.IReadWritePool;
import com.example.indoor.resources.IResource;

public class ReadWriteRing<T> implements IReadWritePool<T>, Iterable<IResource<T>> {
	private Outlet<T> mReadOutlet, mWriteOutlet;
	protected Resource<T> mFirstResource;
	protected int mCapacity = 0;
	
	public ReadWriteRing( int capacity,  IFactory<T> factory ) {
		Resource<T> current = null;
		int remaining = capacity;
		
		boolean isLast = true;
		Resource<T> last = null;
		
		// Generating the ring from the last element to the first, so we
		// already have created the successor. The only exception is the
		// last element in the ring, therefore we save it separately.
		while( remaining > 0 ) {
			current = new Resource<T>( factory.create(), current );
			if( isLast ) {
				isLast = false;
				last = current;
			}
			remaining--;
		}
		
		// closing the ring
		mFirstResource = current;
		last.mNext = mFirstResource;
		
		mCapacity = capacity;
		
		mWriteOutlet = new Outlet<T>(
				mFirstResource,
				Resource.State.AVAILABLE,
				Resource.State.WRITING,
				Resource.State.FILLED );
		
		mReadOutlet = new Outlet<T>(
				mFirstResource,
				Resource.State.FILLED,
				Resource.State.READING,
				Resource.State.AVAILABLE );
	}

	@Override
	public IOutlet<T> getWriteOutlet() {
		return mWriteOutlet;
	}

	@Override
	public IOutlet<T> getReadOutlet() {
		return mReadOutlet;
	}
	
	public int getCapacity() {
		return mCapacity;
	}
	
	private class Iterator implements java.util.Iterator<IResource<T>> {
		private Resource<T> mCurrent;

		Iterator( Resource<T> start ) {
			mCurrent = start;
		}
		
		@Override
		public boolean hasNext() {
			return mCurrent.mNext != mFirstResource;
		}

		@Override
		public IResource<T> next() {
			if( !hasNext() )
				throw new NoSuchElementException();
			mCurrent = mCurrent.mNext;
			return mCurrent;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException( "Removing elements from the ring is not supported." );
		}
		
	}

	@Override
	public java.util.Iterator<IResource<T>> iterator() {
		return new Iterator( mFirstResource );
	}

}
