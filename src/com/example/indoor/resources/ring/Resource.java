package com.example.indoor.resources.ring;

import com.example.indoor.resources.IResource;

public class Resource<T> implements IResource<T> {
	
	protected enum State {
		AVAILABLE,
		WRITING,
		FILLED,
		READING
	};
	
	Outlet<T> mOutlet = null;
	protected T mContents = null;
	protected Resource<T> mNext = null;
	
	State mState = State.AVAILABLE;
	
		
	Resource( T contents, Resource<T> next ) {
			mContents = contents;
			mNext = next;
	}
	
	@Override
	public T getContents() {
		return mContents;
	}
	
	@Override
	public void release() {
		State state = mOutlet.mReleasedState;
		mOutlet = null;
		setState( state );
	}
	
	void setNext( Resource<T> next ) {
		mNext = next;
	}
	
	synchronized void setState( Resource.State state ) {
		mState = state;
		this.notifyAll();
	}
	
	synchronized void waitForState( Resource.State state ) throws InterruptedException {
		while( mState != state ) {
			this.wait();
		}
	}
	
	boolean hasState( Resource.State state ) {
		return mState == state;
	}
}
