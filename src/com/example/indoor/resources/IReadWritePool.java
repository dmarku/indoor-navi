package com.example.indoor.resources;

public interface IReadWritePool<T> {
	IOutlet<T> getWriteOutlet();
	IOutlet<T> getReadOutlet();
}
