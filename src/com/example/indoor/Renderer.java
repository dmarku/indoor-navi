package com.example.indoor;

import org.opencv.core.Mat;

import com.example.gl.Color;
import com.example.gl.Matrix;
import com.example.gl.SensorVector;
import com.example.gl.Vector;
import com.example.gl.ObservableVector.VectorChangeListener;
import com.example.gl.drawable.DrawableBundle;
import com.example.gl.drawable.LineDrawable;
import com.example.gl.drawable.TransformDrawable;
import com.example.gl.drawable.VectorLineDrawable;
import com.example.gl.transform.MatrixTransform;
import com.example.gl.transform.Translation;
import com.example.gl.view.SceneView;
import com.example.indoor.gl.ArrowDrawable;
import com.example.indoor.gl.ScaledResourceDisplay;
import com.example.indoor.resources.IOutlet;

import android.graphics.Bitmap;
import android.hardware.SensorManager;

public class Renderer extends com.example.gl.Renderer {
	
	
	public Renderer( IOutlet<Mat> previewOutlet, Bitmap bitmap, final SensorVector gravity, final SensorVector magnetism ) {
		
	final Matrix mSensorMatrix = new Matrix();
    
    magnetism.addListener(new VectorChangeListener() {
		
		@Override
		public void onVectorChange(Vector vector) {
			Vector.ortho(vector.getValues(), magnetism.getValues(), vector.getValues());
			Vector.normalize(vector.getValues(), vector.getValues());
			SensorManager.getRotationMatrix(
					mSensorMatrix.getValues(),
					null,
					gravity.getValues(),
					magnetism.getValues());
		}
	});
    
    
    gravity.addListener(new VectorChangeListener() {
		
		@Override
		public void onVectorChange(Vector vector) {
			SensorManager.getRotationMatrix(
					mSensorMatrix.getValues(),
					null,
					gravity.getValues(),
					magnetism.getValues());
		}
	});
    
    ScaledResourceDisplay resourceDisplay = new ScaledResourceDisplay( previewOutlet );
    resourceDisplay.setBitmap(bitmap);
    resourceDisplay.setArea(-1f, 1f, -1f, 1f);
    
    LineDrawable gravityDrawable = new VectorLineDrawable(gravity);
    gravityDrawable.setColor(Color.RED);
    gravityDrawable.useColor(true);
    
    LineDrawable magnetismDrawable = new VectorLineDrawable(magnetism);
    magnetismDrawable.setColor(Color.BLUE);
    magnetismDrawable.useColor(true);
    
    DrawableBundle sensorBundle = new DrawableBundle(
    		resourceDisplay,
    		gravityDrawable,
    		magnetismDrawable );
    
    MyRenderView renderView = new MyRenderView();
    renderView.setRootDrawable(sensorBundle);
    
    SceneView sceneView = new SceneView();
    sceneView.setDepth(0.1f, 10f);
    sceneView.setRootDrawable(new TransformDrawable(
    		new Translation(0f, 0f, -5f),
    		new TransformDrawable(
    				new MatrixTransform(mSensorMatrix),
    				new ArrowDrawable(.5f, .65f, 2.5f, 1.6f, 1f, 3.4f, 0.25f))
    		));
    
    addRenderView(renderView);
    addRenderView(sceneView);
		
	}
}
