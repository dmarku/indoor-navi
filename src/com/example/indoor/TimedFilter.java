package com.example.indoor;

import org.opencv.core.Mat;
import org.opencv.core.Size;

import com.example.indoor.cv.IFilter;

/** Decorator class to measure processing times on filters transparently. */
public class TimedFilter implements IFilter {
	
	private IFilter mFilter = null;
	private ITimer mTimer = null;
	
	public TimedFilter( ITimer timer ) {
		mTimer = timer;
		timer.reset();
	}
	
	public TimedFilter( IFilter filter, ITimer timer ) {
		mFilter = filter;
		mTimer = timer;
		timer.reset();
	}
	
	public void setFilter( IFilter filter ) {
		mFilter = filter;
		mTimer.reset();
	}

	@Override
	public void resize( Size size ) {
		mFilter.resize( size );
	}

	@Override
	public void cleanup() {
		mFilter.cleanup();
	}

	@Override
	public Size getPreviewSize() {
		return mFilter.getPreviewSize();
	}

	@Override
	public void process(Mat input, Mat preview) {
		mTimer.start();
		mFilter.process( input, preview );
		mTimer.stop();
	}

	@Override
	public String getName() {
		return mFilter.getName();
	}

}
