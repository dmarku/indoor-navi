package com.example.indoor;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class SimpleTimeText extends TextView implements ITimer {
	
	long init = System.currentTimeMillis(), start = init, end = init;

	public SimpleTimeText( Context context ) {
		super( context );
	}
	public SimpleTimeText( Context context, AttributeSet attrs ) {
		super( context, attrs );
	}
	public SimpleTimeText( Context context, AttributeSet attrs, int defStyle ) {
		super( context, attrs, defStyle );
	}

	@Override
	public void reset() {
		init = System.currentTimeMillis();
		start = init;
		end = init;
		setText("----");
	}
	
	@Override
	public void start() {
		start = System.currentTimeMillis();
	}
	
	@Override
	public void stop() {
		end = System.currentTimeMillis();
		final Long difference = end - start;
		this.post( new Runnable() {
			
			@Override
			public void run() {
				setText( difference.toString() + "ms" );
			}
			
		} );
	}
}
