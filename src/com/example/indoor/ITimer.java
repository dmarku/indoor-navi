package com.example.indoor;

public interface ITimer {
	public void start();
	public void stop();
	
	public void reset();
}
