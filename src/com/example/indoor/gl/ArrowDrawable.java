package com.example.indoor.gl;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.Color;
import com.example.gl.drawable.GeometryDrawable;

public class ArrowDrawable extends GeometryDrawable {

	/**
     * Generates a new arrow geometry ( => ).
     * The arrow is placed flat onto the xy-plane and points towards the
     * positive y-axis. It is centered on the origin.
     * 
     * @param frontHeight	height of the tip from the ground up
     * @param backHeight	height of the back from the ground up
     * @param wingspan		the width of the "wings"
     * @param backWidth		the width of the back end
     * @param jointWidth	the width of the neck joint
     * @param length		total length of the arrow
     * @param ratio			ratio between neck and tip. 0 = no neck, 1 = no tip
     */
	public ArrowDrawable(
    		float frontHeight, float backHeight,
    		float wingspan, float backWidth, float jointWidth,
    		float length, float ratio) {
		super(new Arrow(frontHeight, backHeight, wingspan, backWidth, jointWidth, length, ratio));
		setPrimitiveMode(GL10.GL_TRIANGLES);
		setColor(Color.GREEN);
		setColorMode(ColorMode.VERTEX_COLOR);
	}
}
