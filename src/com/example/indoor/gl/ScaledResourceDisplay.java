package com.example.indoor.gl;

import javax.microedition.khronos.opengles.GL10;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.gl.drawable.ScaledBitmapDisplay;
import com.example.indoor.resources.IOutlet;
import com.example.indoor.resources.IResource;

public class ScaledResourceDisplay extends ScaledBitmapDisplay {
	private IOutlet<Mat> mInput;
	private Bitmap mBitmap = null;
	
	public ScaledResourceDisplay( IOutlet<Mat> input ) {
		super( null );
		mInput = input;
	}

	@Override
	public void draw( GL10 gl ) {
		if( mInput != null ) {
			IResource<Mat> resource = mInput.poll();
			if( resource != null ) {
				Mat image = resource.getContents();
				
				if( mBitmap == null ||
						mBitmap.getWidth() != image.width() ||
						mBitmap.getHeight() != image.height() ) {
					if( mBitmap != null ) {
						mBitmap.recycle();
					}
					mBitmap = Bitmap.createBitmap( image.width(), image.height(), Bitmap.Config.ARGB_8888 );
				}
				
				Utils.matToBitmap( image, mBitmap );
				setBitmap( mBitmap );
				Log.v( "indoor.Display", "Displayed a frame, releasing" );
				resource.release();
			}
		}
		
		super.draw(gl);
	}
}
