package com.example.indoor.gl;

import com.example.gl.Geometry;

public class Arrow extends Geometry {

	/**
     * Generates a new arrow geometry ( => ).
     * The arrow is placed flat onto the xy-plane and points towards the
     * positive y-axis. It is centered on the origin.
     * 
     * @param frontHeight	height of the tip from the ground up
     * @param backHeight	height of the back from the ground up
     * @param wingspan		the width of the "wings"
     * @param backWidth		the width of the back end
     * @param jointWidth	the width of the neck joint
     * @param length		total length of the arrow
     * @param ratio			ratio between neck and tip. 0 = no neck, 1 = no tip
     */
	public Arrow(
    		float frontHeight, float backHeight,
    		float wingspan, float backWidth, float jointWidth,
    		float length, float ratio)
    {
    	float jointHeight = ratio*frontHeight + (1-ratio)*backHeight;
    	float backY = -length*0.5f;
    	float frontY = length*0.5f;
    	float jointY = ratio*frontY + (1-ratio)*backY;
    	
    	float backX = backWidth*0.5f;
    	float jointX = jointWidth*0.5f;
    	float wingX = wingspan*0.5f;
    	
    	setVertexCoords(
    			// tip
    			0f, frontY, 0f, 1f,
    			0f, frontY, frontHeight, 1f,
    			// left/right wings
    			-wingX, jointY, 0f, 1f,
    			-wingX, jointY, jointHeight, 1f,
    			wingX, jointY, 0f, 1f,
    			wingX, jointY, jointHeight, 1f,
    			// left/right joints
    			-jointX, jointY, 0f, 1f,
    			-jointX, jointY, jointHeight, 1f,
    			jointX, jointY, 0f, 1f,
    			jointX, jointY, jointHeight, 1f,
    			// left/right back
    			-backX, backY, 0f, 1f,
    			-backX, backY, backHeight, 1f,
    			backX, backY, 0f, 1f,
    			backX, backY, backHeight, 1f);
    	
    	setColorValues(
    			0f, 0.3f, 0f, 1f,
    			0f, 1f, 0f, 1f,
    			0f, 0.3f, 0f, 1f,
    			0f, 1f, 0f, 1f,
    			0f, 0.3f, 0f, 1f,
    			0f, 1f, 0f, 1f,
    			0f, 0.3f, 0f, 1f,
    			0f, 1f, 0f, 1f,
    			0f, 0.3f, 0f, 1f,
    			0f, 1f, 0f, 1f,
    			0f, 0.3f, 0f, 1f,
    			0f, 1f, 0f, 1f,
    			0f, 0.3f, 0f, 1f,
    			0f, 1f, 0f, 1f);
    	
    	setIndices(new short[] {
    			// bottom, tip
    			0,4,8, 0,6,2, 0,8,6,
    			// bottom, neck
    			8,12,6, 6,12,10,
    			// top, tip
    			1,3,7, 1,9,5, 1,7,9,
    			// top, neck
    			9,7,13, 13,7,11,
    			
    			// sides
    			1,5,0, 0,5,4,
    			4,5,9, 4,9,8,
    			8,9,13, 8,13,12,
    			12,13,11, 12,11,10,
    			10,11,7, 10,7,6,
    			6,7,3, 6,3,2,
    			2,3,1, 2,1,0});
    }
}
