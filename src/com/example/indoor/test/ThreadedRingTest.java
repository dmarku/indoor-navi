package com.example.indoor.test;

/**
 * A small application to test the (blocking) functionality of the
 * ReadWriteRing class.
 * 
 * @author Markus Dittmann
 *
 */

public class ThreadedRingTest {
	
	static final int RING_CAPACITY = 5;
	
	public static void main( String args[] ) {
		ThreadedRingTest test = new ThreadedRingTest( RING_CAPACITY );
		test.run();
	}
	
	public ThreadedRingTest( int capacity ) {
		
	}
	
	public void run() {
		
	}
}
