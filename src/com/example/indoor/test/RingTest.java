package com.example.indoor.test;

import java.io.IOException;

import com.example.indoor.resources.IFactory;
import com.example.indoor.resources.IOutlet;
import com.example.indoor.resources.IReadWritePool;
import com.example.indoor.resources.IResource;
import com.example.indoor.resources.ring.ReadWriteRing;

/**
 * Small application to test the (non-blocking) functionality of the
 * ReadWriteRing class.
 * 
 * @author Markus Dittmann
 *
 */

public class RingTest {
	
	static final int RING_CAPACITY = 5;
	
	IReadWritePool<Integer> mPool = null;
	
	IOutlet<Integer> mWriteOutlet = null;
	IOutlet<Integer> mReadOutlet = null;
	
	IResource<Integer> mWriteResource = null;
	IResource<Integer> mReadResource = null;
	
	public static void main( String args[] ) {
		
		// System.out.println( "[Input] Specify ring capacity" );
		//???
		
		RingTest test = new RingTest( RING_CAPACITY );
		test.run();
		
		System.out.println( "Done testing. Bye :)" );
	}

	
	public RingTest( int capacity ) {
		initialize( capacity );
	}
	
	public void run() {
		System.out.println( "Running Test:" );
		
		int input = 0;
		try {
			while( input != 'x' ) {
				input = getUserInput();
				
				switch( input ) {
				case 'w':
					write();
					break;
				case 'r':
					read();
					break;
				case 'x':
					System.out.println( "Exiting..." );
					continue;
				}
			}
		} catch( IOException e ) {
			System.out.println( e );
		}
	}
	
	private void initialize( final int capacity ) {
		System.out.println( "Initializing..." );
		
		mPool = new ReadWriteRing<Integer>( capacity, new IFactory<Integer>() {
			int counter = capacity;
			
			public Integer create() {
				System.out.println( "Adding " + counter );
				return new Integer(counter--);
			}
		} );
		
		mWriteOutlet = mPool.getWriteOutlet();
		mReadOutlet = mPool.getReadOutlet();
	}
	
	private int getUserInput() throws IOException {
		System.out.println( "[Input] (w)rite, (r)ead or e(x)it?" );
		
		int input = System.in.read();
		while( input == '\r' || input == '\n' )
			input = System.in.read();
		
		return input;
	}
	
	private void write() {
		mWriteResource = mWriteOutlet.poll();
		if( mWriteResource == null ) {
			System.out.println( "[Write] Could not acquire resource for writing." );
		}
		else {
			System.out.println( "[Write] writing to " + mWriteResource.getContents() );
			mWriteResource.release();
			mWriteResource = null;
		}
	}
	
	private void read() {
		mReadResource = mReadOutlet.poll();
		if( mReadResource == null ) {
			System.out.println( "[Read ] Could not acquire resource for reading." );
		}
		else {
			System.out.println( "[Read ] Reading from " + mReadResource.getContents() );
			mReadResource.release();
			mReadResource = null;
		}
	}
}
